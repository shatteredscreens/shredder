﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using Microsoft.Win32;


namespace Shredder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string REGISTRY_KEY = "Software\\ShatteredScreens\\Shredder";

        public MainWindow()
        {
            InitializeComponent();

            // Attempt to open the key.
            RegistryKey key = Registry.CurrentUser.OpenSubKey(REGISTRY_KEY);

            // If the return value is null, the key doesn't exist
            if (key != null)
            {
                // Grab all the details of the last run from the registry.
                if (key.GetValue("OutputFolder") != null)
                    txtOutputFolder.Text = key.GetValue("OutputFolder").ToString();
                if (key.GetValue("ArticyMasterXML") != null)
                    txtArticyMasterXML.Text = key.GetValue("ArticyMasterXML").ToString();
            }
        
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            // Attempt to open the key.
            RegistryKey key = Registry.CurrentUser.OpenSubKey(REGISTRY_KEY, true);

            // If the return value is null, the key doesn't exist
            if (key == null)
            {
                // The key doesn't exist; create it / open it
                key = Registry.CurrentUser.CreateSubKey(REGISTRY_KEY);
            }

            // Grab all the details of the last run from the registry.
            key.SetValue("OutputFolder", txtOutputFolder.Text);
            key.SetValue("ArticyMasterXML", txtArticyMasterXML.Text);

            Close();
        }


        private void ArticyMasterXML_Click(object sender, RoutedEventArgs e)
        {
            // Use the common dialogs to select the folder.
            System.Windows.Forms.OpenFileDialog fileBrowserDialog = new System.Windows.Forms.OpenFileDialog();
            fileBrowserDialog.Filter = "XML Documents (*.xml)|*.xml|All files (*.*)|*.*";
            fileBrowserDialog.FilterIndex = 1;
            fileBrowserDialog.CheckPathExists = true;
            if (fileBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtArticyMasterXML.Text = fileBrowserDialog.FileName;
            else
                txtArticyMasterXML.Text = "";
        }


        private void OutputFolder_Click(object sender, RoutedEventArgs e)
        {
            // Use the common dialogs to select the folder.
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = "Select the directory in which to place the generated files.";
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                txtOutputFolder.Text = folderBrowserDialog.SelectedPath;
            else
                txtOutputFolder.Text = "";
        }


    }
}
